from . import views
from django.urls import path


app_name = 'storydelapan'

urlpatterns = [
    path('', views.index, name="index"),
    path('library/', views.library, name="library")
]
