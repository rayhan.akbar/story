from django.test import TestCase, Client
from django.urls import resolve
from .views import index, library

# Create your tests here.


class TestStoryDelapanIndex(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/storydelapan/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/storydelapan/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/storydelapan/')
        self.assertTemplateUsed(response, 'story-8.html')


class TestStoryDelapanLibrary(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/storydelapan/library/?q=book')
        self.assertEqual(response.status_code, 200)

    def test_event_text_is_exist(self):
        response = Client().get('/storydelapan/library/?q=frozen')
        text = response.content.decode('utf8')
        self.assertIn("food", text)
