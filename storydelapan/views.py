from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.


def index(request):
    return render(request, 'story-8.html')


def library(request):

    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)

    data = json.loads(ret.content)

    return JsonResponse(data, safe=False)

    # req = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + book)
    # data = req.json()
    # data_dump = json.dumps(data)

    # return HttpResponse(content=data_dump, content_type="application/json")
