from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
        ]

        widgets = {
            'username': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter your username'
                }
            ),

            'email': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Email Address'
                }
            ),

            'pass': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Email Address'
                }
            ),

            'password1': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Input password'
                }
            ),

            'password2': forms.PasswordInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Confirm password'
                }
            ),
        }
