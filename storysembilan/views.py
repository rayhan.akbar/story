from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
# Create your views here.
from django.contrib.auth.decorators import login_required


@login_required(login_url='/storysembilan/signin/')
def landing(request):
    # context = {}
    return render(request, 'story-9.html')


def signin(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/storysembilan/')
        else:
            messages.info(request, 'Username or Password is incorrect')

    # context = {}
    return render(request, 'signin.html')


def signup(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(
                request, 'Account successfully created for ' + user)

            return redirect('/storysembilan/signin/')

    context = {'form': form}
    return render(request, 'signup.html', context)


@login_required
def signout(request):
    logout(request)
    return redirect('/storysembilan/signin/')
