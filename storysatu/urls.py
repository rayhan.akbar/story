
from django.urls import path
from . import views

urlpatterns = [
    # urls patterns at settings 'storysatu/'+'' = 'storysatu/'
    path('', views.index, name="index")
]
