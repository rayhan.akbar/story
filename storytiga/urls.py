
from django.urls import path
from . import views

urlpatterns = [
    # urls patterns settings ''+'' = ''
    path('', views.index, name="index"),
    # urls patterns settings ''+'soon/' = 'soon/'
    # path('soon/', views.soon, name="soon")
]
