from django.apps import AppConfig


class StorytigaConfig(AppConfig):
    name = 'storytiga'
