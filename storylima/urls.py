from django.urls import path

from . import views

app_name = 'storylima'

urlpatterns = [
    path('', views.index, name="index"),
    path('create/', views.create, name="create"),
    path('delete/<int:id_input>/', views.delete, name='delete'),
    # path('update/<int:id_update>', views.update, name='update'),
    path('detail/<int:id_detail>/', views.detail, name='detail'),
]
