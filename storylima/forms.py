from django import forms
from .models import Schedule
from django.forms import TextInput, Select, Textarea


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = [
            'lesson',
            'lecturer',
            'credit',
            'description',
            'year',
        ]

        widgets = {
                'lesson': forms.TextInput(
                    attrs={
                        'class': 'form-control',
                        'placeholder': 'Course name'}
                ),

                'lecturer': forms.TextInput(
                    attrs={
                        'class': 'form-control',
                        'placeholder': 'Lecturer name'
                    }
                ),
                'credit': forms.Select(
                    attrs={
                        'class': 'form-control',
                        'placeholder': 'Number of Credits'
                    }
                ),
                'year': forms.Select(
                    attrs={
                        'class': 'form-control',
                        'placeholder': 'Periods of Year'
                    }
                ),
                'description': forms.Textarea(
                    attrs={
                        'class': 'form-control',
                        'placeholder': 'Description'
                    }
                )
        }
