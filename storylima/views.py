from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import Schedule


def create(request):
    object_matkul = ScheduleForm(request.POST or None)

    if request.method == 'POST':
        if object_matkul.is_valid():
            object_matkul.save()

        return redirect('/storylima')

    context = {
        'title': 'Make Schedule',
        'object_matkul': object_matkul,
    }

    return render(request, 'form.html', context)


def index(request):
    object_matkul = Schedule.objects.all()

    context = {
        'title': 'Make Schedule',
        'object_matkul': object_matkul,
    }

    return render(request, 'result.html', context)


def detail(request, id_detail):
    id_matkul = Schedule.objects.get(id=id_detail)
    return render(request, 'detail.html', {'i': id_matkul})


def delete(request, id_input):
    Schedule.objects.filter(id=id_input).delete()
    return HttpResponseRedirect("/storylima")
