from . import views
from django.urls import path


app_name = 'storytujuh'

urlpatterns = [
    path('', views.index, name="index"),
]
