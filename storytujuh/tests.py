

from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.


class TestStoryTujuh(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/storytujuh/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/storytujuh/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/storytujuh/')
        self.assertTemplateUsed(response, 'story-7.html')
