from django import forms
from .models import Kegiatan, Peserta


class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = (
            'nama_kegiatan',
        )

    widgets = {
        'nama_kegiatan': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Activity name'}
        ),
    }


class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = (
            'nama_peserta',
        )

    widgets = {
        'nama_peserta': forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Your name'}
        ),
    }
