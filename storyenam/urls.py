from . import views
from django.urls import path


app_name = 'storyenam'

urlpatterns = [
    path('', views.index, name="index"),
    path('create-activity/', views.createActivity, name="createActivity"),
    path('create-name/<int:id_nama>/', views.createName, name="createName"),
    path('delete/<int:id_activity>/', views.delete, name='delete'),
    path('deleteOrang/<int:id_orang>/', views.deleteOrang, name='deleteOrang')
]
